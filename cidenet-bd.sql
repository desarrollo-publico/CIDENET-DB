--BD CIDENET

CREATE TABLE pais (
    codigo INT PRIMARY KEY,	
    nombre VARCHAR(50) NOT NULL,
) TYPE = INNODB;

CREATE TABLE tipo_identificacion (
    id INT PRIMARY KEY,	
    nombre VARCHAR(50) NOT NULL,
) TYPE = INNODB;

CREATE TABLE estado_empleado (
    id INT PRIMARY KEY,	
    estado VARCHAR(50) NOT NULL,
) TYPE = INNODB;

CREATE TABLE area (
    id INT PRIMARY KEY,	
    nombre VARCHAR(50) NOT NULL,
) TYPE = INNODB;


CREATE TABLE empleado (
    id INT AUTO_INCREMENT PRIMARY KEY,	
    primer_apellido VARCHAR(20) NOT NULL UNIQUE,
    segundo_apellido VARCHAR(20) NOT NULL,
    primer_nombre VARCHAR(20) NOT NULL,
    otros_nombre VARCHAR(50) NULL,
	codigo_pais INT NOT NULL,
	numero_dentificación VARCHAR(20) NOT NULL,
	id_tipo_identificacion INT NOT NULL,
    correo VARCHAR(300) NOT NULL,	
    fecha_ingreso TIMESTAMP NOT NULL,
	id_area INT NOT NULL,
	id_estado_emplado INT NOT NULL,
    fecha_registro TIMESTAMP NOT NULL,		
    INDEX (codigo_pais),
    FOREIGN KEY (codigo_pais) REFERENCES pais(codigo),
    INDEX (id_tipo_identificacion),
    FOREIGN KEY (id_tipo_identificacion) REFERENCES tipo_identificacion(id),,
    INDEX (id_area),
    FOREIGN KEY (id_area) REFERENCES area(id),
    INDEX (id_estado_emplado),
    FOREIGN KEY (id_estado_emplado) REFERENCES estado_empleado(id)
) TYPE = INNODB;